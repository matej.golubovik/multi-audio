# Multiaudio Video
Instructions for playing a dual audio video

First you need to have a dual audio video, instructions on how to create such a video can be found here:

https://www.techoize.com/make-dual-audio-movie-video/

To implement the created video with the program you need to put the video and the main.html file in the same folder.
Also, the main.html file needs to be changed. The source of the video needs to be changed to the video we want to be displayed.
When opening the video in the browser (Microsoft Edge or Internet Explorer) there is a button that changes the audio of the video in real time.

Percent:
Arsen Matej Golubovikj: 36%
Predrag Zvezdakovski: 32%
Igor Saveski: 32%
